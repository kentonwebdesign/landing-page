<?php

$EmailFrom = "artur@kentonwebdesign.com";
$EmailTo = "ab@kentonwd.com";
$Subject = "TORBRAM STEELES Interest";
$Name = Trim(stripslashes($_POST['Full_Name'])); 
$Email = Trim(stripslashes($_POST['Email'])); 
$Phone = Trim(stripslashes($_POST['Phone_Number'])); 
$Referral = Trim(stripslashes($_POST['Referral'])); 
$Intention = Trim(stripslashes($_POST['Intended_use'])); 
$Size = Trim(stripslashes($_POST['Desired_size'])); 
$Timing = Trim(stripslashes($_POST['Purchase_Timing'])); 

// validation
$validationOK=true;
if (!$validationOK) {
  print "<meta http-equiv=\"refresh\" content=\"0;URL=error.htm\">";
  exit;
}

// prepare email body text
$Body = "";
$Body .= "Full Name: ";
$Body .= $Name;
$Body .= "\n";
$Body .= "Email: ";
$Body .= $Email;
$Body .= "\n";
$Body .= "Phone Number: ";
$Body .= $Phone;
$Body .= "\n";
$Body .= "How did you hear about us: ";
$Body .= $Referral;
$Body .= "\n";
$Body .= "Intended Use: ";
$Body .= $Intention;
$Body .= "\n";
$Body .= "Desired Size: ";
$Body .= $Size;
$Body .= "\n";
$Body .= "Purchase Timing: ";
$Body .= $Timing;
$Body .= "\n";

// send email 
$success = mail($EmailTo, $Subject, $Body, "From: <$EmailFrom>");

// redirect to success page 
if ($success){
	print "<meta http-equiv=\"refresh\" content=\"0;URL=../thank-you.html\">";
}
else{
  print "<p>Please Return and Correct Your Submission</p>";
}
?>